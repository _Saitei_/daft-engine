
#ifndef Other_API_H
#define Other_API_H

#ifdef Other_BUILT_AS_STATIC
#  define Other_API
#  define OTHER_NO_EXPORT
#else
#  ifndef Other_API
#    ifdef Other_EXPORTS
        /* We are building this library */
#      define Other_API __declspec(dllexport)
#    else
        /* We are using this library */
#      define Other_API __declspec(dllimport)
#    endif
#  endif

#  ifndef OTHER_NO_EXPORT
#    define OTHER_NO_EXPORT 
#  endif
#endif

#ifndef OTHER_DEPRECATED
#  define OTHER_DEPRECATED __declspec(deprecated)
#endif

#ifndef OTHER_DEPRECATED_EXPORT
#  define OTHER_DEPRECATED_EXPORT Other_API OTHER_DEPRECATED
#endif

#ifndef OTHER_DEPRECATED_NO_EXPORT
#  define OTHER_DEPRECATED_NO_EXPORT OTHER_NO_EXPORT OTHER_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define OTHER_NO_DEPRECATED
#endif

#endif
