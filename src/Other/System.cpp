#include "System.h"
#include <iostream>

System::System(const std::string& system_name): system_name(system_name)
{}

void System::Log(const std::string& log_text)
{
	std::cout<<"["<<system_name<<"] "<<log_text<<std::endl;
}

System::~System(void)
{
}
