#pragma once
#include <string>
#include "OTHER_Export.h"

class Other_API System
{
private:
	const std::string system_name;
public:
	System(const std::string& system_name);
	void Log(const std::string& log_text);
	virtual ~System(void) = 0;
};

