#pragma once
#include <glew.h>
#include "../Core/Core.h"
#include "RENDERER_Export.h"
#include <string>

class RendererAPI Shader
{
private:
	GLuint id;
	const GLenum type;
	std::string source;
public:
	Shader(const GLenum shader_type);
	GLuint GetID() { return id; }
	std::string& GetSource() { return source; }
	void LoadFromString(const std::string& src);
	void LoadFromFile(const std::string& filename);
	bool Compile();
private:
	void Log(const std::string& text);
};

