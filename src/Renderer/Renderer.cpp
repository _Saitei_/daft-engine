#include "Renderer.h"
#include <glew.h>

bool Renderer::Init()
{
	glewExperimental = true;
	GLenum err = glewInit();
	if(err != GLEW_OK)
	{
		Log("Error: " + std::string(reinterpret_cast<char const*>(glewGetErrorString(err))));
		return false;
	}
	else
	{
		Log("GLEW version: " + GetGlewVersion());
		Log("OpenGL version: " + GetGlVersion());
		if (!GLEW_VERSION_3_3)
		{
			Log("Error: OpenGL 3.3 is not supported!");
			return false;
		}
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glFrontFace(GL_CW);
	glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH_CLAMP);
	glEnable(GL_FRAMEBUFFER_SRGB);

	Log("Initialized");
	return true;
}

void Renderer::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::SubsystemLog(const std::string & from, const std::string & text)
{
	Log("[" + from + "] " + text);
}

std::string Renderer::GetGlewVersion()
{
	return std::string(reinterpret_cast<char const*>(glewGetString(GLEW_VERSION)));
}

std::string Renderer::GetGlVersion()
{
	return std::string(reinterpret_cast<char const*>(glGetString(GL_VERSION)));
}

Renderer::~Renderer()
{
	Log("Destroyed");
}
