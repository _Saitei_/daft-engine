#pragma once
#include "../Other/System.h"
#include "RENDERER_Export.h"
#include <string>

class RendererAPI Renderer: System
{
public:
	Renderer() : System("Renderer") {}
	~Renderer();
	bool Init();
	void ClearScreen();
private:
	friend class Shader;
	friend class Program;
	void SubsystemLog(const std::string& from, const std::string& text);
	std::string GetGlewVersion();
	std::string GetGlVersion();
};

