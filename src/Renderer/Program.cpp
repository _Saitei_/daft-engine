#include "Program.h"
#include <stdexcept>

Program::Program(): shader_count(0)
{
	id = glCreateProgram();
	Log("Created");
}

void Program::AttachShader(Shader& shader)
{
	glAttachShader(id, shader.GetID());
	++shader_count;
	Log("Shader attached");
}

bool Program::Link()
{
	GLint status = GL_FALSE;

	if(shader_count >= 2)
	{
		glLinkProgram(id);

		glGetProgramiv(id, GL_LINK_STATUS, &status);
		if(status == GL_FALSE)
		{
			GLint info_log_length;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &info_log_length);

			GLchar *str_info_log = new GLchar[info_log_length + 1];
			glGetProgramInfoLog(id, info_log_length, NULL, str_info_log);

			Log("Linking failed: " + std::string(str_info_log));

			delete[] str_info_log;
		}
		else Log("Linking OK");
	}
	else
	{
		Log("Can't link shaders - you need at least 2");
	}

	return status != false;
}

void Program::Enable()
{
	glUseProgram(id);
}

void Program::Disable()
{
	glUseProgram(0);
}

GLuint Program::Attribute(const std::string & attribute)
{
	auto it = attribute_loc_list.find(attribute);

	if(it != attribute_loc_list.end()) //Found it! Return the bound location
	{
		return attribute_loc_list[attribute];
	}
	else //error? 
	{
		Log("Can't found attribute");
		throw std::runtime_error("Can't found attribute");
	}
}

GLuint Program::Uniform(const std::string& uniform)
{
	auto it = uniform_loc_list.find(uniform);

	if (it != uniform_loc_list.end()) //Found it! Return the bound location
	{
		return uniform_loc_list[uniform];
	}
	else //error? 
	{
		Log("Can't found uniform");
		throw std::runtime_error("Can't found uniform");
	}
}

GLuint Program::AddAttribute(const std::string& attribute)
{
	GLint err = glGetAttribLocation(id, attribute.c_str());
	if(err == -1)
	{
		Log("Could not add attribute: " + attribute);
		throw std::runtime_error("Could not add attribute");
	}

	attribute_loc_list[attribute] = err;
	return err;
}

GLuint Program::AddUniform(const std::string& uniform)
{
	GLint err = glGetUniformLocation(id, uniform.c_str());
	if (err == -1) //Error!
	{
		Log("Could not add uniform: " + uniform);
		throw std::runtime_error("Could not add uniform");
	}

	uniform_loc_list[uniform] = err;
	return err;
}

void Program::Log(const std::string& text)
{
	/*Core::GetInstance()->GetRenderer()->SubsystemLog("Program " +
		std::to_string(id), text);*/
}

Program::~Program()
{
	glDeleteProgram(id);
	Log("Destroyed");
}