#pragma once
#include <glew.h>
#include "RENDERER_Export.h"

class RendererAPI Buffer //VBO
{
private:
	GLuint id;
	static GLuint current_id;
public:
	Buffer();
	~Buffer();
	GLuint GetID() { return id; }
	void Bind(GLenum target = GL_ARRAY_BUFFER);
	void Unbind(GLenum target = GL_ARRAY_BUFFER);
	void SetData(GLsizeiptr size, const void* data, GLenum target = GL_ARRAY_BUFFER);
};

