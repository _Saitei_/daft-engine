#include "Shader.h"
#include <fstream>
#include <sstream>

Shader::Shader(const GLenum shader_type): type(shader_type)
{
	if (type != GL_VERTEX_SHADER && type != GL_FRAGMENT_SHADER
		&& type != GL_GEOMETRY_SHADER)
	{
		Log("Unknown shader type: " + std::to_string(type));
	}
	else
	{
		id = glCreateShader(type);
		Log("Created");
	}
}

void Shader::LoadFromString(const std::string & src)
{
	source = src;
	const char* source_pointer = source.c_str();
	glShaderSource(id, 1, &source_pointer, NULL);
	Log("Source loaded from string");
}

void Shader::LoadFromFile(const std::string & filename)
{
	std::ifstream file(filename.c_str(), std::ios::in);
	if(!file.good())
	{
		Log("Unable to open file: " + filename);
		return;
	}

	std::string line;
	while(std::getline(file, line))
	{
		source += line + "\n";
	}
	file.close();

	const char* source_pointer = source.c_str();
	glShaderSource(id, 1, &source_pointer, NULL);
	Log("Source loaded from file: " + filename);
}

bool Shader::Compile()
{
	glCompileShader(id);

	GLint status = GL_FALSE;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		GLint info_log_length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &info_log_length);

		GLchar *str_info_log = new GLchar[info_log_length + 1];
		glGetShaderInfoLog(id, info_log_length, NULL, str_info_log);

		Log("Compilation failed: " + std::string(str_info_log));

		delete[] str_info_log;
	}
	else Log("Compilation OK");

	return status != false;
}

void Shader::Log(const std::string & text)
{
	/*Core::GetInstance()->GetRenderer()->SubsystemLog("Shader #" + std::to_string(id),
		text);*/
}