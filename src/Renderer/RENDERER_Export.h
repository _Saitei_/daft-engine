
#ifndef RendererAPI_H
#define RendererAPI_H

#ifdef Renderer_BUILT_AS_STATIC
#  define RendererAPI
#  define RENDERER_NO_EXPORT
#else
#  ifndef RendererAPI
#    ifdef Renderer_EXPORTS
        /* We are building this library */
#      define RendererAPI __declspec(dllexport)
#    else
        /* We are using this library */
#      define RendererAPI __declspec(dllimport)
#    endif
#  endif

#  ifndef RENDERER_NO_EXPORT
#    define RENDERER_NO_EXPORT 
#  endif
#endif

#ifndef RENDERER_DEPRECATED
#  define RENDERER_DEPRECATED __declspec(deprecated)
#endif

#ifndef RENDERER_DEPRECATED_EXPORT
#  define RENDERER_DEPRECATED_EXPORT RendererAPI RENDERER_DEPRECATED
#endif

#ifndef RENDERER_DEPRECATED_NO_EXPORT
#  define RENDERER_DEPRECATED_NO_EXPORT RENDERER_NO_EXPORT RENDERER_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define RENDERER_NO_DEPRECATED
#endif

#endif
