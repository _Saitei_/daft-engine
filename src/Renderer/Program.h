#pragma once
#include <glew.h>
#include "../Core/Core.h"
#include "RENDERER_Export.h"
#include "Shader.h"
#include <string>
#include <map>

class RendererAPI Program
{
private:
	GLuint id;
	GLuint shader_count;
	std::map<std::string, int> attribute_loc_list;
	std::map<std::string, int> uniform_loc_list; 
public:
	Program();
	~Program();
	GLuint GetID() { return id; }
	void AttachShader(Shader& shader);
	bool Link();
	void Enable();
	void Disable();
	GLuint Attribute(const std::string& attribute);
	GLuint Uniform(const std::string& uniform);
	GLuint AddAttribute(const std::string& attribute);
	GLuint AddUniform(const std::string& uniform);
private: 
	void Log(const std::string& text);
};

