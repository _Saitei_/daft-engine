#pragma once
#include "RENDERER_Export.h"

class RendererAPI Vertex
{
public:
	Vertex() : x(0.0f), y(0.0f), z(0.0f) {}
	Vertex(float x, float y, float z);
	float GetX() { return x; }
	float GetY() { return y; }
	float GetZ() { return z; }
	void SetX(float x) { this->x = x; }
	void SetY(float y) { this->y = y; }
	void SetZ(float z) { this->z = z; }
private:
	float x, y, z;
};

