#pragma once
#include "RENDERER_Export.h"
#include <glew.h>

class RendererAPI VertexArray
{
private:
	GLuint id;
	static GLuint current_id;
public:
	VertexArray();
	~VertexArray();
	void Bind();
	void Unbind();
};

