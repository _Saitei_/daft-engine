#include "Buffer.h"

GLuint Buffer::current_id = 0;

Buffer::Buffer()
{
	glGenBuffers(1, &id);
}

void Buffer::Bind(GLenum target)
{
	glBindBuffer(target, id);
	current_id = id;
}

void Buffer::Unbind(GLenum target)
{
	if(current_id == id)
	{
		glBindBuffer(target, 0);
		current_id = 0;
	}
}

void Buffer::SetData(GLsizeiptr size, const void* data, GLenum target)
{
	GLuint tmp_id = current_id;
	Bind(target);
	glBufferData(target, size, data, GL_STATIC_DRAW);
	glBindBuffer(target, tmp_id);
	current_id = tmp_id;
}

Buffer::~Buffer()
{
	if(current_id == id)
	{
		current_id = 0;
	}

	glDeleteBuffers(1, &id);
}
