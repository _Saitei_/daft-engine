#include "VertexArray.h"

GLuint VertexArray::current_id = 0;

VertexArray::VertexArray()
{
	glGenBuffers(1, &id);
}

void VertexArray::Bind()
{
	current_id = id;
	glBindVertexArray(id);
}

void VertexArray::Unbind()
{
	if(current_id == id)
	{
		current_id = 0;
	}
	glBindVertexArray(0);
}

VertexArray::~VertexArray()
{
	Unbind();
	glDeleteBuffers(1, &id);
}
