#pragma once
#include <list>
#include <memory>
#include <string>
#include "../../Other/System.h"
#include "Window.h"
#include "../CORE_Export.h"
#include "../InputManager/InputManager.h"
#include "../Core.h"

class CoreAPI WindowManager: System
{
private:
	std::list<std::unique_ptr<Window>>windows;
	InputManager *input_manager;
	Core *core;
public:
	WindowManager(Core *core);
	~WindowManager();
    	WindowManager(const WindowManager&) = delete;
    	WindowManager& operator=(const WindowManager&) = delete;
	void Update();
	void PushWindow(Window* window);
private:
	friend class Window;
	void SubsystemLog(const std::string& from, const std::string& text);
};

