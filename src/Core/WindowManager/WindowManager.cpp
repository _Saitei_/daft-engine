/*#include "../../Renderer/Buffer.h"
#include "../../Renderer/VertexArray.h"
#include "../../Renderer/Shader.h"
#include "../../Renderer/Program.h"*/

#include "WindowManager.h"
#include <thread>

WindowManager::WindowManager(Core *core): System("WindowManager")
{
	this->core = core;
	input_manager = core->GetInputManager();
	Log("Initialized");
}

void WindowManager::Update()
{
	/*VertexArray vao;
	vao.Bind();
	Buffer vbo;
	Buffer vbo_color;
	const GLfloat g_vertex_buffer_data[] = 
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f,
	};	
	vbo.Bind();
	vbo.SetData(sizeof(g_vertex_buffer_data), g_vertex_buffer_data);
	const GLfloat color[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
	};
	vbo_color.Bind();
	vbo_color.SetData(sizeof(color), color);
	


	Shader vs(GL_VERTEX_SHADER), fs(GL_FRAGMENT_SHADER);
	Program prog;
	vs.LoadFromFile("vertex_shader.txt");
	vs.Compile();
	fs.LoadFromFile("fragment_shader.txt");
	fs.Compile();
	prog.AttachShader(vs);
	prog.AttachShader(fs);
	prog.Link();

	prog.AddAttribute("pos");
	prog.AddAttribute("col");

	glEnableVertexAttribArray(prog.Attribute("pos"));
	glEnableVertexAttribArray(prog.Attribute("col"));
	vbo.Bind();
	glVertexAttribPointer(prog.Attribute("pos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
	vbo_color.Bind();
	glVertexAttribPointer(prog.Attribute("col"), 3, GL_FLOAT, GL_FALSE, 0, 0);
	prog.Enable();
	//glViewport(0, 0, 100, 100);*/

	
	while (!windows.empty())
	{
		for (auto it = windows.begin(); it != windows.end();)
		{
			Window *window = it->get();
			//window->SetCurrent();

			if (window->IsVisible())
			{
				input_manager->SetCurrentWindow(window);
				if (window->ShouldClose() || input_manager->IsKeyPressed(KEY_ESCAPE))
				{
					delete window;
					it->release();
					it = windows.erase(it);
					continue;
				}
				else ++it;

				core->GetRenderer()->ClearScreen();
				/*...*/
				//glDrawArrays(GL_TRIANGLES, 0, 3);
				window->SwapBuffers();
				glfwPollEvents();
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(2));
	}

	//glDisableVertexAttribArray(0);
	//vao.Unbind();
}

void WindowManager::PushWindow(Window* window)
{
	window->SetWindowManager(this);
	window->Log("Created");
	std::unique_ptr<Window> win(window);
	windows.push_back(std::move(win));
}

void WindowManager::SubsystemLog(const std::string& from, const std::string& text)
{
	Log(" [" + from + "] " + text);
}

WindowManager::~WindowManager()
{
	Log("Destroyed");
}
