#pragma once
#include <GLFW/glfw3.h>
#include <string>
#include "../../Other/defs.h"
#include "../CORE_Export.h"
#include "WindowManager.h"

class CoreAPI Window
{
private:
	unsigned int id;
	GLFWwindow* window;
	class WindowManager* window_manager;
public:
	Window(unsigned int window_width, unsigned int window_height, 
		std::string title);
	~Window();
	void SetCurrent();
	void Destroy();
	unsigned int GetID(){return id;}
	void SwapBuffers();
	bool ShouldClose();
	bool IsFocused(); //input focus
	bool IsVisible();
	bool IsIconified();
	bool IsResizable();
	void Show();
	void Hide();
private:
	friend class Keyboard;
	friend class Mouse;
	friend class WindowManager;
	GLFWwindow* GetGlfwWindow() { return window; }
	void SetWindowManager(class WindowManager *window_manager);
	void Log(std::string text);
};