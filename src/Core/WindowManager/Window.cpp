#include "Window.h"
#include <stdexcept>
#include <iostream>

Window::Window(unsigned int window_width, unsigned int window_height, 
		std::string title)
{
	static unsigned int id = 0;
	this->id = id++;
	window = glfwCreateWindow(window_width, window_height, title.c_str(),
		NULL, NULL);
	
	if(!window)
	{
		throw std::runtime_error("class Window: can't create window");
	}
}

void Window::SetCurrent()
{
	if(window)
	{
		glfwMakeContextCurrent(window);
	}
	else throw std::runtime_error("class Window: error in Window::SetCurrent. " 
		"Window is already destroyed");
}

void Window::Destroy()
{
	if(window)
	{
		glfwDestroyWindow(window);
		window = nullptr;
		glfwMakeContextCurrent(nullptr);
	}
}

void Window::SwapBuffers()
{
	if(window)
	{
		glfwSwapBuffers(window);
	}
	else throw std::runtime_error("class Window: error in Window::SwapBuffers. "
		"Window is already destroyed");
}


bool Window::ShouldClose() 
{
	return glfwWindowShouldClose(window) != false;
}

bool Window::IsFocused()
{
	return glfwGetWindowAttrib(window, GLFW_FOCUSED) != false;
}

void Window::Show()
{
	glfwShowWindow(window);
}

void Window::Hide()
{
	glfwHideWindow(window);
}

bool Window::IsVisible()
{
	return glfwGetWindowAttrib(window, GLFW_VISIBLE) != false;
}

bool Window::IsIconified()
{
	return glfwGetWindowAttrib(window, GLFW_ICONIFIED) != false;
}

bool Window::IsResizable()
{
	return glfwGetWindowAttrib(window, GLFW_RESIZABLE) != false;
}

void Window::SetWindowManager(WindowManager *window_manager)
{
	this->window_manager = window_manager;
}

void Window::Log(std::string text)
{
	window_manager->SubsystemLog("Window #" + std::to_string(id), text);
}

Window::~Window()
{
	Destroy();
	Log("Destroyed");
}