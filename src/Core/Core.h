#pragma once
#include "../Renderer/Renderer.h"
#include "WindowManager/Window.h"
#include "../Other/System.h"
#include "InputManager/InputManager.h"
#include "WindowManager/WindowManager.h"
#include "CORE_Export.h"
#include <memory>
#include <string>
#include <vector>

class CoreAPI Core: System
{
private:
	std::unique_ptr<class InputManager> input_manager;
	std::unique_ptr<class WindowManager> window_manager;
	std::unique_ptr<class Renderer> renderer;
	bool all_okay;
public:
	Core(void);
	~Core(void);
	Core(const Core&) = delete;
	Core& operator=(const Core&) = delete;
	bool Init(class Window *window);
	void Run();
	static Core* GetInstance();
	InputManager* GetInputManager();
	WindowManager* GetWindowManager();
	Renderer* GetRenderer();
private:
	static void ErrorCallback(int error, const char* description);
};