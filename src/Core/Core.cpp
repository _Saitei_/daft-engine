#include "Core.h"
#include <stdexcept>
#include <iostream>
#include <thread>

Core::Core(void): System("Core"), all_okay(false)
{
	if (!glfwInit())
	{
		Log("glfwInit() error");
	}
	else all_okay = true;

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwSetErrorCallback(ErrorCallback);
}

void Core::ErrorCallback(int error, const char* description)
{
	std::cout<<"[Core] "<<description<<std::endl; //�������
}

bool Core::Init(Window *window)
{	
	if (!all_okay)
	{
		return false;
	}
	else all_okay = false;

	try
	{
		input_manager = std::make_unique<InputManager>(this);
		window_manager = std::make_unique<WindowManager>(this);
		window->SetCurrent();
		GetWindowManager()->PushWindow(window);
		renderer = std::make_unique<Renderer>();
	}
	catch(std::runtime_error &e)
	{
		Log(e.what());
		return false;
	}
	catch(...)
	{
		Log("Error in Core::Init()");
		return false;
	}


	if(!renderer->Init())
	{
		Log("Error in Core::Init(): Renderer::Init() fails!");
		return false;
	}

	Log("Initialized");
	all_okay = true;
	return true;
}

void Core::Run()
{
	if (!all_okay)
	{
		Log("Error in Core::Run(), because Core::Init() fails");
		return;
	}
	window_manager->Update(); //Main Loop
}

Core* Core::GetInstance()
{
	static Core* core = new Core();
	return core;
}

InputManager* Core::GetInputManager()
{
	return input_manager.get();
}

WindowManager* Core::GetWindowManager()
{
	return window_manager.get();
}

Renderer* Core::GetRenderer()
{
	return renderer.get();
}

Core::~Core(void)
{
	glfwTerminate();
	Log("Destroyed");
}