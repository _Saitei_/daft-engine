#include "InputManager.h"
#include <string>

InputManager::InputManager(Core *core): current_window(nullptr), System("InputManager")
{
	this->core = core;
	try
	{
		mouse = std::make_unique<Mouse>(this);
		keyboard = std::make_unique<Keyboard>(this);
	}
	catch(...)
	{
		Log("Error in InputManager::InputManager()");
		throw std::runtime_error("Can't initialize InputManager");
	}
	Log("Initialized");
}

void InputManager::SetCurrentWindow(Window *current_window)
{
	//Log("Current window: " + std::to_string(current_window->GetID()));
	this->current_window = current_window;
}

bool InputManager::IsKeyPressed(int key)
{
	return keyboard->IsKeyPressed(key, current_window);
}

bool InputManager::IsMouseButtonPressed(int button)
{
	return mouse->IsMouseButtonPressed(button, current_window);
}

void InputManager::SubsystemLog(std::string from, std::string text)
{
	Log("[" + from + "] " + text);
}

InputManager::~InputManager(void)
{
	Log("Destroyed");
}