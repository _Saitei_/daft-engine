#pragma once
#include "../WindowManager/Window.h"
#include "GLFW/glfw3.h"
#include "InputManager.h"
#include "../CORE_Export.h"
#include <memory>
#include <string>

class CoreAPI Mouse
{
private:
	class InputManager *input_manager;
public:
	Mouse(class InputManager *input_manager);
	~Mouse(void);
	bool IsMouseButtonPressed(int button, class Window *current_window);
	bool IsMouseButtonReleased(int button, class Window *current_window);
private:
	void Log(std::string text);
};

