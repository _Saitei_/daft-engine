#include "Keyboard.h"

Keyboard::Keyboard(InputManager *input_manager)
{
	this->input_manager = input_manager;
	Log("Initialized");
}

bool Keyboard::IsKeyPressed(int key, Window *current_window)
{
	if(glfwGetKey(current_window->GetGlfwWindow(), key) == GLFW_PRESS)
	{
		Log("Key pressed: " + std::to_string(key));
		return true;
	}
	else return false;
}

bool Keyboard::IsKeyReleased(int key, Window *current_window)
{
	if(glfwGetKey(current_window->GetGlfwWindow(), key) == GLFW_RELEASE)
	{
		Log("Key released: " + std::to_string(key));
		return true;
	}
	else return false;
}

void Keyboard::Log(std::string text)
{
	input_manager->SubsystemLog("Keyboard", text);
}

Keyboard::~Keyboard(void)
{
	Log("Destroyed");
}