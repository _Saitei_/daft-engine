#pragma once
#include <GLFW/glfw3.h>
#include <string>
#include <memory>
#include "../WindowManager/Window.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "../CORE_Export.h"
#include "../../Other/System.h"
#include "../Core.h"

class CoreAPI InputManager: System
{
private:
	std::unique_ptr<class Keyboard> keyboard;
	std::unique_ptr<class Mouse> mouse;
	Window *current_window; 
	class Core *core;
public:
	InputManager(Core *core);
	~InputManager(void);
	void SetCurrentWindow(Window *current_window);
	bool IsKeyPressed(int key);
	bool IsMouseButtonPressed(int button);
private:
	friend class Keyboard;
	friend class Mouse;
	void SubsystemLog(std::string from, std::string text);
};

