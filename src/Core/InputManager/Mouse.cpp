#include "Mouse.h"


Mouse::Mouse(InputManager *input_manager)
{
	this->input_manager = input_manager;
	Log("Initialized");
}

bool Mouse::IsMouseButtonPressed(int button, Window *current_window)
{
	if(glfwGetMouseButton(current_window->GetGlfwWindow(), button) == GLFW_PRESS)
	{
		Log("Mouse button pressed: " + std::to_string(button));
		return true;
	}
	else return false;
}

bool Mouse::IsMouseButtonReleased(int button, Window *current_window)
{
	if(glfwGetMouseButton(current_window->GetGlfwWindow(), button) == GLFW_RELEASE)
	{
		Log("Mouse button released: " + std::to_string(button));
		return true;
	}
	else return false;
}

void Mouse::Log(std::string text)
{
	input_manager->SubsystemLog("Mouse", text);
}

Mouse::~Mouse(void)
{
	Log("Destroyed");
}