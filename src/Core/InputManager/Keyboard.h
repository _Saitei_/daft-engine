#pragma once
#include "../WindowManager/Window.h"
#include "GLFW/glfw3.h"
#include "InputManager.h"
#include "../CORE_Export.h"
#include <memory>
#include <string>

class CoreAPI Keyboard
{
private:
	class InputManager* input_manager;
public:
	Keyboard(class InputManager *input_manager);
	~Keyboard(void);
	bool IsKeyPressed(int key, class Window *current_window);
	bool IsKeyReleased(int key, class Window *current_window);
private:
	void Log(std::string text);
};

