
#ifndef CoreAPI_H
#define CoreAPI_H

#ifdef Core_BUILT_AS_STATIC
#  define CoreAPI
#  define CORE_NO_EXPORT
#else
#  ifndef CoreAPI
#    ifdef Core_EXPORTS
        /* We are building this library */
#      define CoreAPI __declspec(dllexport)
#    else
        /* We are using this library */
#      define CoreAPI __declspec(dllimport)
#    endif
#  endif

#  ifndef CORE_NO_EXPORT
#    define CORE_NO_EXPORT 
#  endif
#endif

#ifndef CORE_DEPRECATED
#  define CORE_DEPRECATED __declspec(deprecated)
#endif

#ifndef CORE_DEPRECATED_EXPORT
#  define CORE_DEPRECATED_EXPORT CoreAPI CORE_DEPRECATED
#endif

#ifndef CORE_DEPRECATED_NO_EXPORT
#  define CORE_DEPRECATED_NO_EXPORT CORE_NO_EXPORT CORE_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define CORE_NO_DEPRECATED
#endif

#endif
