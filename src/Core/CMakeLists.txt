project(DaftEngine)

set(CMAKE_CURRENT_LIST_DIR ${CMAKE_CURRENT_SOURCE_DIR})

file(GLOB_RECURSE PROJECT_SOURCE_FILES "*.h" "*.hpp" "*.cpp")
GroupSources(src/Core)

add_library(Core SHARED ${PROJECT_SOURCE_FILES})
GENERATE_EXPORT_HEADER( 
	Core
      	BASE_NAME Core
       	EXPORT_MACRO_NAME CoreAPI
       	EXPORT_FILE_NAME CORE_Export.h
       	STATIC_DEFINE Core_BUILT_AS_STATIC
)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../lib/glfw ${CMAKE_CURRENT_SOURCE_DIR}/lib/glfw)
target_link_libraries(Core glfw ${GLFW_LIBRARIES} Other Renderer)