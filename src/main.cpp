#include "Core/Core.h"
#include <iostream>

int main(void)
{
	{
		Core* core = Core::GetInstance();
		core->Init(new Window(480, 480, "1"));
		std::cout<<core<<std::endl;
		std::cout << Core::GetInstance() << std::endl;
		core->Run();
	}
	system("PAUSE");
	return 0;
}