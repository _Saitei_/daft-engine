cmake_minimum_required(VERSION 2.8.16)
project(DaftEngine)
include (GenerateExportHeader)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
set(OUTPUT_PATH ${CMAKE_BINARY_DIR})

macro(copy_src SrcDir FolderNameInBuild)
	file(GLOB SRC ${SrcDir}/*)
	file(COPY ${SRC} DESTINATION ${FolderNameInBuild})
endmacro(copy_src)

macro(GroupSources curdir)
	file(GLOB children RELATIVE ${OUTPUT_PATH}/${curdir}
		${OUTPUT_PATH}/${curdir}/*)

	foreach(child ${children})
		if(IS_DIRECTORY ${OUTPUT_PATH}/${curdir}/${child})
			GroupSources(${curdir}/${child})
		else()
			string(REPLACE "/" "\\" groupname ${curdir})
			string(REPLACE "src" "Common" groupname ${groupname})
			source_group(${groupname} FILES ${OUTPUT_PATH}/${curdir}/${child})
		endif()
	endforeach()
endmacro()

copy_src(src "src")
copy_src(lib "lib")

set(SOURCE_EXE ${OUTPUT_PATH}/src/main.cpp)
set(CMAKE_MODULE_PATH ${OUTPUT_PATH}/cmake)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${OUTPUT_PATH}/lib)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${OUTPUT_PATH}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_PATH}/bin)

include_directories(${OUTPUT_PATH})
include_directories(${OUTPUT_PATH}/src)
include_directories(${OUTPUT_PATH}/lib/glfw/include)
include_directories(${OUTPUT_PATH}/lib/glew-1.12.0/include)
include_directories(${OUTPUT_PATH}/lib/glew-1.12.0/include/GL)

add_executable(Launcher ${SOURCE_EXE})
add_subdirectory(${OUTPUT_PATH}/src/Other)
add_subdirectory(${OUTPUT_PATH}/src/Core)
add_subdirectory(${OUTPUT_PATH}/src/Renderer)

target_link_libraries(Launcher Core)